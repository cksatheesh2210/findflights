package com.satheesh.findflights;

import static org.junit.Assert.assertEquals;
import java.text.ParseException;
import java.util.ArrayList;

import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.ui.ModelMap;

import com.satheesh.findflights.controllers.FlightController;
import com.satheesh.findflights.entities.Flight;

@RunWith(SpringRunner.class)
@SpringBootTest
class TestControllerMethods {

	@Autowired
	FlightController flightController;
	
	@Test
	void testShowFindFlightsPage() {
		assertEquals( "findFlights",  flightController.showFindFlightsPage());
	}

	/*
	 * This test runs fine with the data loaded into database given with the project.
	 * In a real world scenario you would run the test on a separate database, where you have to create the entries within the test case. 
	 */
	@SuppressWarnings("unchecked")
	@Test
	void testFindFlights() throws ParseException {		
		ModelMap modelMap = new ModelMap();
		
		String result = flightController.findFlights("MUC", "FRA", "12-05-2019", modelMap);
		
		assertEquals( "displayFlights",  result);
		assertEquals(1, modelMap.size());
		assertEquals(3, ((ArrayList<Flight>) modelMap.get("flights")).size());
	}
}
