package com.satheesh.findflights;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FindFlightsApplication {

	public static void main(String[] args) {
		SpringApplication.run(FindFlightsApplication.class, args);
	}

}
